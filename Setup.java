import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Setup extends JFrame{
    private JButton card1 = new JButton();
    private JButton card2 = new JButton();
    private JButton card3 = new JButton();
    private JButton card4 = new JButton();
    private JButton card5 = new JButton();
    private JButton card6 = new JButton();
    private JButton card7 = new JButton();
    private JButton card8 = new JButton();
    private JButton card9 = new JButton();
    private JButton card10 = new JButton();
    private JButton card11 = new JButton();
    private JButton card12 = new JButton();
    private JButton card13 = new JButton();
    private JButton card14 = new JButton();
    private JButton card15 = new JButton();
    private JButton card16 = new JButton();
    private Round round;
    private JTextArea cScoreText; 
    private JTextArea hScoreText;

    private GameController controller;
    private Position pos;
    private Turn turn;
    public Turn getTurn(){ return this.turn;}

    public Setup(GameController controller)
    {
        this.round = controller.getRound();
        this.pos = controller.getPos();
        this.turn = controller.getTurn();
        // *** NO GUI STUFF IN THE CONSTRUCTOR IN ORDER TO GIVE THE CONSTRUCTOR TIME TO FINISH !!! ****
    }


    public void runTheGameAfterConstructorHasFinished()
    {
        cScoreText = new JTextArea("Current score: " + round.getCurrentScore());
        hScoreText = new JTextArea("High score: " + round.getHighScore());


       JFrame frame = new JFrame("Matching Game");
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setLayout(null);
       frame.setSize(1000,1000);
       card1.setBounds(20,20,100,60);
       card2.setBounds(140,20,100,60);
       card3.setBounds(260,20,100,60);
       card4.setBounds(380,20,100,60);
       card5.setBounds(20,90,100,60);
       card6.setBounds(140,90,100,60);
       card7.setBounds(260,90,100,60);
       card8.setBounds(380,90,100,60);
       card9.setBounds(20,160,100,60);
       card10.setBounds(140,160,100,60);
       card11.setBounds(260,160,100,60);
       card12.setBounds(380,160,100,60);
       card13.setBounds(20,230,100,60);
       card14.setBounds(140,230,100,60);
       card15.setBounds(260,230,100,60);
       card16.setBounds(380,230,100,60);
       JButton checkButton = new JButton("Check for match");
       checkButton.setBounds(500,20,150,60);
       frame.getContentPane().add(card1); // Adds buttons to content pane of frame
       frame.getContentPane().add(card2);
       frame.getContentPane().add(card3);
       frame.getContentPane().add(card4);
       frame.getContentPane().add(card5); 
       frame.getContentPane().add(card6);
       frame.getContentPane().add(card7);
       frame.getContentPane().add(card8);
       frame.getContentPane().add(card9);
       frame.getContentPane().add(card10);
       frame.getContentPane().add(card11);
       frame.getContentPane().add(card12);
       frame.getContentPane().add(card13); 
       frame.getContentPane().add(card14);
       frame.getContentPane().add(card15);
       frame.getContentPane().add(card16);
       frame.getContentPane().add(checkButton);

       card1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e1) {
            card1.setText(pos.getAssigment(1));
            getTurn().addChoice(1);

        }
    });
        card2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e2) {
            card2.setText(pos.getAssigment(2));
            getTurn().addChoice(2);

        }
    });
        card3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e3) {
            card3.setText(pos.getAssigment(3));
            getTurn().addChoice(3);

        }
    });
        card4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e4) {
            card4.setText(pos.getAssigment(4));
            getTurn().addChoice(4);

        }
    });
        card5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e5) {
            card5.setText(pos.getAssigment(5));
            getTurn().addChoice(5);

        }
    });
        card6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e6) {
            card6.setText(pos.getAssigment(6));
            getTurn().addChoice(6);

        }
    });
        card7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e7) {
            card7.setText(pos.getAssigment(7));
            getTurn().addChoice(7);
        }
    });
        card8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e8) {
            card8.setText(pos.getAssigment(8));
            getTurn().addChoice(8);
        }
    });
        card9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e9) {
            card9.setText(pos.getAssigment(9));
            getTurn().addChoice(9);
        }
    });
        card10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e10) {
            card10.setText(pos.getAssigment(10));
            getTurn().addChoice(10);
        }
    });
        card11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e11) {
            card11.setText(pos.getAssigment(11));
            getTurn().addChoice(11);
        }
    });
        card12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e12) {
            card12.setText(pos.getAssigment(12));
            getTurn().addChoice(12);
        }
    });
        card13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e13) {
            card13.setText(pos.getAssigment(13));
            getTurn().addChoice(13);
        }
    });

        card14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e14) {
            card14.setText(pos.getAssigment(14));
            getTurn().addChoice(14);
        }
    });
        card15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e15) {
            card15.setText(pos.getAssigment(15));
            getTurn().addChoice(15);
        }
    });
        card16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e16) {
            card16.setText(pos.getAssigment(16));
            getTurn().addChoice(16);
        }
    });
        checkButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
        int choice1 = turn.getChoice(0);
        int choice2 = turn.getChoice(1);
        turn.checkMatch(choice1, choice2);
        }
    });

       
       hScoreText.setWrapStyleWord(true);
       hScoreText.setEditable(false);
       hScoreText.setWrapStyleWord(true);
       frame.getContentPane().add(hScoreText);
       
 
       cScoreText.setWrapStyleWord(true);
       cScoreText.setEditable(false);
       cScoreText.setWrapStyleWord(true);
       frame.getContentPane().add(cScoreText);

        pos.startPos();
        pos.setPos();
       frame.setVisible(true);
    }
        public void resetCards(int c)
    {
            switch (c) {
                case 1:
                  card1.setText("");
                  break;
                case 2:
                  card2.setText("");
                  break;
                case 3:
                  card3.setText("");
                  break;
                case 4:
                  card4.setText("");
                  break;
                case 5:
                  card5.setText("");
                  break;
                case 6:
                  card6.setText("");
                  break;
                case 7:
                  card7.setText("");
                  break;
                case 8:
                  card8.setText("");
                  break;
                case 9:
                  card9.setText("");
                  break;
                case 10:
                  card10.setText("");
                  break;
                case 11:
                  card12.setText("");
                  break;
                case 13:
                  card13.setText("");
                  break;
                case 14:
                  card14.setText("");
                  break;
                case 15:
                  card15.setText("");
                  break;
                case 16:
                  card16.setText("");
                  break;
            }
        
    }
    public void setHighScore(int h)
    {
        hScoreText.setText("High Score: " + h);
    }
    public void setCurrentScore(int c)
    {
        hScoreText.setText("High Score: " + c);
    }

    public void resetAll()
    {
        card1.setText("");
        card2.setText("");
        card3.setText("");
        card4.setText("");
        card5.setText("");
        card6.setText("");
        card7.setText("");
        card8.setText("");
        card9.setText("");
        card10.setText("");
        card11.setText("");
        card12.setText("");
        card13.setText("");
        card14.setText("");
        card15.setText("");
        card16.setText("");
    }
    public void resetAllCards()
    {
        resetAll();
    }
}
