public class GameController
{
    public Position getPos()
    {
        return pos;
    }

    public Round getRound()
    {
        return round;
    }

    public Turn getTurn()
    {
        return turn;
    }

    public Setup getSetup()
    {
        return setup;
    }

    private Position pos;
    private Round round;
    private Turn turn;
    private Setup setup;

    public GameController()
    {
        this.pos = new Position(this);
        this.round = new Round(this);
        this.turn = new Turn(this);
        System.out.println("DEBUG: GameController created an instance of Turn");
        this.setup = new Setup(this);
    }
}

