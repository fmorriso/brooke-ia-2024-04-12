import java.util.*;

public class Turn 
{
    private  ArrayList<Integer> choices;
    private GameController controller;
    private Round round;
    private Position pos;
    private Setup setup;
    private Turn turn;

    public Turn(GameController controller)
    {
        choices = new ArrayList<Integer>();
        this.controller = controller;

        round = controller.getRound();
        pos = controller.getPos();
        setup = controller.getSetup();
        turn = controller.getTurn();
    }

    public void addChoice(int choice)
    {
        choices.add(choice);
    }

    public ArrayList<Integer> getChoices()
    {
        return choices;
    }

    public void resetChoices()
    {
        choices.clear();
    }

    public int getChoice(int c)
    {
        int choice = choices.get(c);
        return choice;
    }
        

    public void checkMatch(int pos1, int pos2)
    {
        if(turn == null) {
            System.out.println("WARNING: instance variable turn is NULL, so method calls will probably fail");
        }
        if (pos.getAssigment(pos1) == pos.getAssigment(pos2)) {
            round.incScore();
            this.resetChoices();
            round.incMatches();
            setup.setCurrentScore(round.getCurrentScore());
            if (round.getMatches() > 7) {
                round.endRound();
            }
        } else {
            round.decScore();
            controller.getTurn().resetChoices();
            //WARNING: setup.resetCards(); // <--- NEEDS AN INT ARGUMENT TO COMPILE - ZERO ????
            controller.getSetup().resetAllCards(); // Mr. Morrison's guess at what might be needed
            controller.getSetup().setCurrentScore(round.getCurrentScore());
        }
    }

}
